Workflow Buttons
================

Provide workflow buttons for content moderation instead of a select 
dropdown of states.  Button labels are taken from the transition names.

 * https://www.drupal.org/project/workflow_buttons
 * Issues: https://www.drupal.org/project/issues/workflow_buttons
 * Source code: https://git.drupalcode.org/project/workflow_buttons.git
 * Keywords: 
   * drupal
   * editor
   * workflow
   * buttons
   * form
   * content
   * content moderation
 * Package name: drupal/workflow_buttons


### Requirements

No dependencies.


### License

GPL-2.0+

### Setup instructions

Set the form widget of Moderation state pseudo-field to Workflow buttons.

### Translation

To translate buttons, translate the workflow transition labels of the core 
Workflows module at `/admin/config/regional/config-translation/workflow`
